import UIKit

final class ScoreView: UIView {
  private let titleLabel = UILabel()
  private let scoreLabel = UILabel()
  private let subtitleLabel = UILabel()
  let containerView = CircleContainerView()
  private let stackView = UIStackView()
  private let spinner = UIActivityIndicatorView(style: .large)
  
  private func arrangeColors() {
    switch traitCollection.userInterfaceStyle {
    case .dark:
      backgroundColor = .black
      titleLabel.textColor = .white
      scoreLabel.textColor = .magenta
      subtitleLabel.textColor = .white
      
    default:
      backgroundColor = .white
      titleLabel.textColor = .black
      scoreLabel.textColor = .orange
      subtitleLabel.textColor = .black
    }
  }
  
  override func traitCollectionDidChange(_ previousTraitCollection: UITraitCollection?) {
    super.traitCollectionDidChange(previousTraitCollection)
    arrangeColors()
  }
  
  func update(viewModel: ScoreViewModel) {
    stackView.arrangedSubviews.forEach(stackView.removeArrangedSubview)
    
    switch viewModel.state {
    case let .loaded(loaded):
      spinner.stopAnimating()
      [titleLabel, scoreLabel, subtitleLabel].forEach(stackView.addArrangedSubview)
      titleLabel.text = loaded.title
      scoreLabel.text = loaded.scoreTitle
      subtitleLabel.text = loaded.subtitle
      
    case .loading:
      spinner.startAnimating()
      stackView.addArrangedSubview(spinner)
      
    case let .failed(error):
      spinner.stopAnimating()
      let failureLabel = UILabel()
      failureLabel.text = error.localizedDescription
      containerView.backgroundColor = .red
      stackView.addArrangedSubview(failureLabel)
    }
  }
  
  init(viewModel: ScoreViewModel) {
    titleLabel.font = .preferredFont(forTextStyle: .body)
    titleLabel.textAlignment = .center
    scoreLabel.font = .preferredFont(forTextStyle: .largeTitle)
    scoreLabel.textColor = .orange
    scoreLabel.textAlignment = .center
    subtitleLabel.font = .preferredFont(forTextStyle: .body)
    subtitleLabel.textAlignment = .center
    
    super.init(frame: .zero)
    
    containerView.addSubview(stackView)
    addSubview(containerView)
    
    stackView.axis = .vertical
    stackView.isUserInteractionEnabled = false
    setupConstraints()
    arrangeColors()
    update(viewModel: viewModel)
  }
  
  private func setupConstraints() {
    stackView.translatesAutoresizingMaskIntoConstraints = false
    containerView.translatesAutoresizingMaskIntoConstraints = false
    titleLabel.translatesAutoresizingMaskIntoConstraints = false
    scoreLabel.translatesAutoresizingMaskIntoConstraints = false
    subtitleLabel.translatesAutoresizingMaskIntoConstraints = false
    
    titleLabel.setContentCompressionResistancePriority(.required, for: .horizontal)
    scoreLabel.setContentCompressionResistancePriority(.required, for: .horizontal)
    subtitleLabel.setContentCompressionResistancePriority(.required, for: .horizontal)
    
    NSLayoutConstraint.activate([
      containerView.widthAnchor.constraint(greaterThanOrEqualTo: stackView.widthAnchor, multiplier: 1.5),
      stackView.centerYAnchor.constraint(equalTo: containerView.centerYAnchor),
      stackView.centerXAnchor.constraint(equalTo: containerView.centerXAnchor),
      containerView.centerYAnchor.constraint(equalTo: centerYAnchor),
      containerView.centerXAnchor.constraint(equalTo: centerXAnchor),
      containerView.heightAnchor.constraint(equalTo: containerView.widthAnchor),
    ])
    
  }
  
  required init?(coder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
}
