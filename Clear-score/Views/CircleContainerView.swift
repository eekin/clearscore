import UIKit

final class CircleContainerView: UIControl {
  init() {
    super.init(frame: .zero)
    accessibilityIdentifier = "detail button"
  }

  override var bounds: CGRect {
    didSet {
      layer.borderWidth = 2
      layer.masksToBounds = true
      layer.cornerRadius = bounds.size.height / 2
      arrangeColors()
    }
  }

  private func arrangeColors() {
    switch traitCollection.userInterfaceStyle {
    case .dark:
      backgroundColor = .black
      layer.borderColor = UIColor.white.cgColor

    default:
      backgroundColor = .white
      layer.borderColor = UIColor.black.cgColor
    }
  }

  override func traitCollectionDidChange(_ previousTraitCollection: UITraitCollection?) {
    super.traitCollectionDidChange(previousTraitCollection)
    arrangeColors()
  }

  required init?(coder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
}
