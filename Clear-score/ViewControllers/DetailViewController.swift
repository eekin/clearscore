import UIKit

final class DetailViewController: UIViewController {
  let clientRefLabel = UILabel()
  let statusLabel = UILabel()
  let percentageCreditUsedLabel = UILabel()
  let viewModel: DetailViewModel

  init(viewModel: DetailViewModel) {
    self.viewModel = viewModel
    super.init(nibName: nil, bundle: nil)
    title = viewModel.screenTitle
  }

  required init?(coder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }

  override func loadView() {
    super.loadView()

    clientRefLabel.text = viewModel.detail.clientRef
    statusLabel.text = viewModel.detail.status
    percentageCreditUsedLabel.text = "\(viewModel.detail.percentageCreditUsed)"
    let stackView = UIStackView(arrangedSubviews: [clientRefLabel, statusLabel, percentageCreditUsedLabel])
    stackView.axis = .vertical
    let _view = UIView()

    view = _view
    view.addSubview(stackView)
    arrangeColors()
    stackView.translatesAutoresizingMaskIntoConstraints = false

    NSLayoutConstraint.activate([
      stackView.leadingAnchor.constraint(equalTo: view.layoutMarginsGuide.leadingAnchor),
      stackView.trailingAnchor.constraint(equalTo: view.layoutMarginsGuide.trailingAnchor),
      stackView.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor)
    ])

  }

  private func arrangeColors() {
    switch traitCollection.userInterfaceStyle {
    case .dark:
      view.backgroundColor = .black
      clientRefLabel.textColor = .white
      statusLabel.textColor = .white
      percentageCreditUsedLabel.textColor = .white

    default:
      view.backgroundColor = .white
      clientRefLabel.textColor = .black
      statusLabel.textColor = .black
      percentageCreditUsedLabel.textColor = .black
    }
  }

  override func traitCollectionDidChange(_ previousTraitCollection: UITraitCollection?) {
    super.traitCollectionDidChange(previousTraitCollection)
    arrangeColors()
  }
}
