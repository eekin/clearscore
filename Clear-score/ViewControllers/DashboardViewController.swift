import UIKit
import Combine

final class DashboardViewController: UIViewController {
  var viewModel: ScoreViewModel {
    didSet{
      guard oldValue != viewModel, let view = (self.view as? ScoreView) else {return}
      view.update(viewModel: self.viewModel)
      guard case .loaded = viewModel.state else{return}
      view.containerView.addTarget(self, action: #selector(self.performTap), for: .touchUpInside)
    }
  }
  
  private let perform: (ScoreViewModel.Action) -> Void
  private let scoreResult: AnyPublisher<Result<Score, APIError>, Never>
  private var cancellable: AnyCancellable?
  
  init(
    viewModel: ScoreViewModel,
    scoreResult: AnyPublisher<Result<Score, APIError>, Never>,
    perform: @escaping (ScoreViewModel.Action) -> Void) {
    self.viewModel = viewModel
    self.perform = perform
    self.scoreResult = scoreResult
    super.init(nibName: nil, bundle: nil)
    title = viewModel.screenTitle
  }

  required init?(coder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
  
  override func viewDidLoad() {
    super.viewDidLoad()
    
    cancellable = scoreResult
      .sink { [weak self] in
        self?.viewModel.update(result: $0)
      }
    
    perform(.pageAppeared)
  }
  
  @objc private func performTap() {
    guard let detailViewModel = viewModel.detailViewModel else {return}
    perform(.tappedDetail(detailViewModel))
  }
  
  override func loadView() {
    super.loadView()
    view = ScoreView(viewModel: viewModel)
  }
}
