import Foundation

protocol APIProtocol {
  func execute<Value: Decodable>(_ request: Request<Value>, completion: @escaping (Result<Value, APIError>) -> Void)
}

enum APIError: Error, LocalizedError {
  case networkError
  case parsingError

  var errorDescription: String? {
    switch self {
    case .networkError:
      return "Network Error!"
    case .parsingError:
      return "Parsing Error!"
    }
  }
}

extension URL {
  func url(with queryItems: [URLQueryItem]) -> URL {
    var components = URLComponents(url: self, resolvingAgainstBaseURL: true)!
    components.queryItems = (components.queryItems ?? []) + queryItems
    return components.url!
  }

  init<Value>(_ host: String, _ request: Request<Value>) {
    let url = URL(string: host)!
      .appendingPathComponent(request.path)

    self.init(string: url.absoluteString)!
  }
}

struct APIManager: APIProtocol {
  private let host = "https://5lfoiyb0b3.execute-api.us-west-2.amazonaws.com"
  private let urlSession: URLSession

  init(urlSession: URLSession = .shared) {
    self.urlSession = urlSession
  }

  func execute<Value: Decodable>(_ request: Request<Value>, completion: @escaping (Result<Value, APIError>) -> Void) {
    urlSession.dataTask(with: urlRequest(for: request)) { responseData, response, error in
      if let data = responseData {
        let response: Value
        do {
          response = try JSONDecoder().decode(Value.self, from: data)
        } catch {
          completion(.failure(.parsingError))
          return
        }
        completion(.success(response))
      } else {
        completion(.failure(.networkError))
      }
    }.resume()
  }

  private func urlRequest<Value>(for request: Request<Value>) -> URLRequest {
    let url = URL(host, request)
    var result = URLRequest(url: url)
    result.httpMethod = request.method.rawValue
    return result
  }

}

struct MockAPI: APIProtocol {
  private let result: Loadable<Score, APIError>

  init(result: Loadable<Score, APIError>) {
    self.result = result
  }

  func execute<Value>(_ request: Request<Value>, completion: @escaping (Result<Value, APIError>) -> Void) where Value : Decodable {
    switch result {
    case let .loaded(score):
      guard let _score = score as? Value else {return}
      completion(.success(_score))
    case .loading:
      break
    case let .failed(error):
      completion(.failure(error))
    }
  }
}
