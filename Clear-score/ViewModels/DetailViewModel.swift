struct DetailViewModel: Equatable {
  struct Detail: Equatable {
    let clientRef: String
    let status: String
    let percentageCreditUsed: Int
  }

  let screenTitle: String
  let detail: Detail

  init(detail: Detail, screenTitle: String) {
    self.screenTitle = screenTitle
    self.detail = detail
  }
}
