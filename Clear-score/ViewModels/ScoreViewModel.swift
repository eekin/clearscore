struct ScoreViewModel: Equatable {
  enum Action: Equatable {
    case pageAppeared
    case tappedDetail(DetailViewModel)
  }
  
  struct RepresentableScore: Equatable {
    let title: String
    let scoreTitle: String
    let subtitle: String
  }

  mutating func update(result: Result<Score, APIError>) {
    state = Loadable(result.map { score in
      ScoreViewModel.RepresentableScore(
        title: "Your credit score is",
        scoreTitle: score.creditReportInfo.score.description,
        subtitle: "out of \(score.creditReportInfo.maxScoreValue)")
    })

    if case let .success(score) = result {
      detailViewModel = .init(
        detail: .init(
          clientRef: score.creditReportInfo.clientRef,
          status: score.creditReportInfo.status,
          percentageCreditUsed: score.creditReportInfo.percentageCreditUsed),
        screenTitle: "Detail"
      )
    }
  }

  var detailViewModel: DetailViewModel?
  let screenTitle: String
  var state: Loadable<RepresentableScore, APIError>
  
  init(state: Loadable<RepresentableScore, APIError>, screenTitle: String) {
    self.screenTitle = screenTitle
    self.state = state
  }
}
