import UIKit
import Combine

final class SceneDelegate: UIResponder, UIWindowSceneDelegate {
  private let scoreResult = PassthroughSubject<Result<Score, APIError>, Never>()
  var window: UIWindow?
  
  private lazy var api: APIProtocol = {
    var _api: APIProtocol
    let args = ProcessInfo.processInfo.arguments
    if args.contains("success") {
      _api = MockAPI(result: .loaded(Score.mockScore))
    } else if args.contains("failure") {
      _api = MockAPI(result: .failed(.parsingError))
    } else if args.contains("loading") {
      _api = MockAPI(result: .loading)
    } else {
      _api = APIManager()
    }
    
    return _api
  }()
  
  private lazy var dashboardViewController: DashboardViewController = {
    DashboardViewController(
      viewModel: .init(state: .loading, screenTitle: "Dashboard"),
      scoreResult: scoreResult.eraseToAnyPublisher()
    ){ [weak self] action in
      switch action {
      case .pageAppeared:
        self?.api.execute(Score.creditScore) { [weak self] result in
         DispatchQueue.main.async {
            self?.scoreResult.send(result)
        }
        }
        
      case let .tappedDetail(viewModel):
        self?.navigationController.pushViewController(DetailViewController(viewModel: viewModel), animated: true)
      }
    }
  }()
  
  private lazy var navigationController: UINavigationController = {
    let navControl = UINavigationController(rootViewController: dashboardViewController)
    
    navControl.navigationBar.titleTextAttributes = [.foregroundColor: UIColor.white]
    navControl.navigationBar.barStyle = .black
    navControl.navigationBar.isTranslucent = false
    
    return navControl
  }()
  
  func scene(_ scene: UIScene, willConnectTo session: UISceneSession, options connectionOptions: UIScene.ConnectionOptions) {
    guard let windowScene = (scene as? UIWindowScene) else { return }
    
    window = UIWindow(windowScene: windowScene)
    window?.rootViewController = navigationController
    window?.makeKeyAndVisible()
  }
}
