# ClearScore App

## Supported freatures
- Arrange-Act-Assert notation is used in tests
- Dark Mode
- Dynamic Fonts
- Unit tests
- UI Tests
- ELM Architecture, MVVM
- Generics (Polymorphic API calls)
- Reactive Properties (Combine)
- Dependency injection over API
- No Storyboards
- No Singletons
- No Dependencies

## Limitations
- Production code has fixtures and Mock API
- UI Tests don't connect to localhost, instead uses mock api data structure
- Lack of Snapshot tests
- UI Tests launch arguments
- No VoiceOver support
