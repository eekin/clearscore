import XCTest

final class Clear_scoreUITests: XCTestCase {
  private var app: XCUIApplication!

  override func setUpWithError() throws {
    app = XCUIApplication()
    continueAfterFailure = false
  }

  override func tearDownWithError() throws {
    app = nil
  }

  func test_failure() {
    app.launchArguments = ["testMode", "failure"]

    app.launch()

    XCTAssertTrue(app.staticTexts["Parsing Error!"].exists)
    XCTAssertFalse(app.staticTexts["Your credit score is"].exists)
    XCTAssertFalse(app.staticTexts["514"].exists)
    XCTAssertFalse(app.staticTexts["out of 700"].exists)
  }

  func test_success() {
    app.launchArguments = ["testMode", "success"]

    app.launch()

    XCTAssertTrue(app.otherElements["detail button"].exists)
    XCTAssertTrue(app.staticTexts["Your credit score is"].exists)
    XCTAssertTrue(app.staticTexts["514"].exists)
    XCTAssertTrue(app.staticTexts["out of 700"].exists)
  }

  func test_detail() {
    app.launchArguments = ["testMode", "success"]

    app.launch()

    app.otherElements["detail button"].tap()

    XCTAssertTrue(app.staticTexts["CS-SED-655426-708782"].exists)
    XCTAssertTrue(app.staticTexts["MATCH"].exists)
    XCTAssertTrue(app.staticTexts["44"].exists)
  }

  func test_loading() {
    app.launchArguments = ["testMode", "loading"]

    app.launch()

    XCTAssertTrue(app.activityIndicators.firstMatch.exists)
    XCTAssertTrue(app.otherElements["detail button"].exists)
    XCTAssertFalse(app.staticTexts["Your credit score is"].exists)
    XCTAssertFalse(app.staticTexts["514"].exists)
    XCTAssertFalse(app.staticTexts["out of 700"].exists)
  }
}
