
enum Loadable<Value, Failure: Error> {
  case loading
  case loaded(Value)
  case failed(Failure)

  init(_ result: Result<Value, Failure>) {
    switch result {
    case let .success(value):
      self = .loaded(value)
    case let .failure(error):
      self = .failed(error)
    }
  }
}

extension Loadable: Equatable where Value: Equatable, Failure: Equatable {}

