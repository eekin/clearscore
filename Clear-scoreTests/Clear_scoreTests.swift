import XCTest
@testable import Clear_score
import Combine

final class Clear_scoreTests: XCTestCase {
    func test_decode() {
      let score = Score.mockScore

      XCTAssertEqual(score.creditReportInfo.score, 514)
      XCTAssertEqual(score.creditReportInfo.minScoreValue, 0)
      XCTAssertEqual(score.creditReportInfo.maxScoreValue, 700)
    }

  func test_api_success() {
    let score = Score.mockScore
    let api = MockAPI(result: .loaded(score))
    var capturedResults = [Result<Score, APIError>]()

    api.execute(Score.creditScore) {
      capturedResults.append($0)
    }

    XCTAssertEqual(capturedResults, [.success(score)])
  }

  func test_api_failure() {
    let api = MockAPI(result: .failed(.networkError))
    var capturedResults = [Result<Score, APIError>]()

    api.execute(Score.creditScore) {
      capturedResults.append($0)
    }

    XCTAssertEqual(capturedResults, [.failure(.networkError)])
  }

  func test_scoreViewModel_update_success() {
    var vm = ScoreViewModel(state: .loading, screenTitle: "screenTitle")

    vm.update(result: .success(Score.mockScore))

    XCTAssertEqual(
      vm.detailViewModel,
      .init(detail: .init(clientRef: "CS-SED-655426-708782", status: "MATCH", percentageCreditUsed: 44), screenTitle: "Detail")
    )
    XCTAssertEqual(vm.state, .loaded(.init(title: "Your credit score is", scoreTitle: "514", subtitle: "out of 700")))
  }

  func test_scoreViewModel_update_failure() {
    var vm = ScoreViewModel(state: .loading, screenTitle: "screenTitle")

    vm.update(result: .failure(.networkError))

    XCTAssertNil(vm.detailViewModel)
    XCTAssertEqual(vm.state, .failed(.networkError))
  }

  func test_scoreViewModel_failure() {
    let vm = ScoreViewModel(state: .failed(.networkError), screenTitle: "screenTitle")

    XCTAssertEqual(vm.detailViewModel, nil)
    XCTAssertEqual(vm.screenTitle, "screenTitle")
    XCTAssertEqual(vm.state, .failed(.networkError))
  }

  func test_scoreViewModel_success() {
    let vm = ScoreViewModel(state: .loaded(.init(title: "a", scoreTitle: "b", subtitle: "c")), screenTitle: "screenTitle")

    XCTAssertEqual(vm.detailViewModel, nil)
    XCTAssertEqual(vm.screenTitle, "screenTitle")
    XCTAssertEqual(vm.state, .loaded(.init(title: "a", scoreTitle: "b", subtitle: "c")))
  }

  func test_scoreViewModel_loading() {
    let vm = ScoreViewModel(state: .loading, screenTitle: "screenTitle")

    XCTAssertEqual(vm.detailViewModel, nil)
    XCTAssertEqual(vm.screenTitle, "screenTitle")
    XCTAssertEqual(vm.state, .loading)
  }

  func test_detailViewModel() {
    let vm = DetailViewModel(detail: .init(clientRef: "a", status: "b", percentageCreditUsed: 1), screenTitle: "title")

    XCTAssertEqual(vm.detail, .init(clientRef: "a", status: "b", percentageCreditUsed: 1))
    XCTAssertEqual(vm.screenTitle, "title")
  }

  func test_dashboardViewController() {
    let vm = ScoreViewModel(state: .loading, screenTitle: "screenTitle")
    let vc = DashboardViewController(
      viewModel: vm,
      scoreResult: Empty().eraseToAnyPublisher()
    ) { _ in }

    vc.loadViewIfNeeded()

    XCTAssertEqual(vc.viewModel.state, .loading)
  }

  func test_dashboardViewController_captureActions_pageAppeared() {
    var capturedActions = [ScoreViewModel.Action]()
    let vm = ScoreViewModel(state: .loaded(.init(title: "a", scoreTitle: "b", subtitle: "c")), screenTitle: "screenTitle")
    let vc = DashboardViewController(
      viewModel: vm,
      scoreResult: Empty().eraseToAnyPublisher()
    ) { capturedActions.append($0) }

    vc.loadViewIfNeeded()

    XCTAssertEqual(capturedActions, [.pageAppeared])
  }

  func test_dashboardViewController_captureActions_tappedDetail() {
    let scoreResult = PassthroughSubject<Result<Score, APIError>, Never>()
    var capturedActions = [ScoreViewModel.Action]()
    let vm = ScoreViewModel(state: .loaded(.init(title: "a", scoreTitle: "b", subtitle: "c")), screenTitle: "screenTitle")
    let vc = DashboardViewController(
      viewModel: vm,
      scoreResult: scoreResult.eraseToAnyPublisher()
    ) { capturedActions.append($0) }

    vc.loadViewIfNeeded()
    scoreResult.send(.success(Score.mockScore))

    (vc.view as? ScoreView)?.containerView.sendActions(for: .touchUpInside)

    let detailViewModel = DetailViewModel(detail: .init(clientRef: "CS-SED-655426-708782", status: "MATCH", percentageCreditUsed: 44), screenTitle: "Detail")
    XCTAssertEqual(capturedActions, [.pageAppeared , .tappedDetail(detailViewModel)])
  }
}
